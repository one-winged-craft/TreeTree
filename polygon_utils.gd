class_name PolygonUtils
extends Node


static func regular_polygon(sides: int, diameter: int, position := Vector2.ZERO) -> PackedVector2Array:
	#flat side always facing down
	var turn_amount := 360.0 / float(sides)
	var angle := 0.0
	var base_vec := Vector2(0, - (diameter / 2.0))
	var polygon := PackedVector2Array()
	for i in sides:
		polygon.append(position + base_vec.rotated(deg_to_rad(angle)))
		angle += turn_amount
	return polygon


static func rectangle_polygon(width: int, height: int, position := Vector2.ZERO) -> PackedVector2Array:
	var rectangle := PackedVector2Array()
	rectangle.append(position + Vector2(-floor(width / 2), -floor(height / 2)))
	rectangle.append(position + Vector2(ceil(width / 2), -floor(height / 2)))
	rectangle.append(position + Vector2(ceil(width / 2), ceil(height / 2)))
	rectangle.append(position + Vector2(-floor(width / 2), ceil(height / 2)))
	return rectangle


static func rounded_polygon(points: PackedVector2Array, rounding: float) -> PackedVector2Array:
	#ONLY WORKS ON CONVEX EQUIANGULAR POLYGONS
	
	if rounding == 0.0:
		return points
	
	var circle_steps = 10 #arbitrary nb of steps
	var max_rounding := 0.35 #arbitrary limit
	var true_rounding : float = clamp(rounding, 0.0, 1.0) * max_rounding
	var final_points := PackedVector2Array()
	var sides := points.size()
	
	#find shortest side length
	var shortest := INF
	for i in sides:
		var p_next = points[i+1] if i < sides - 1 else points[0]
		var length = points[i].distance_to(p_next)
		if length < shortest:
			shortest = length
	#distance from each vertex where the rounding arc starts
	var offset := shortest * true_rounding
	
	for i in sides:
		#find the 2 segments making up each point
		var p_prev = points[i-1] if i > 0 else points[sides - 1]
		var p_next = points[i+1] if i < sides - 1 else points[0]
		var side_a = p_prev - points[i] 
		var side_b = p_next - points[i]
		
		#find inward orientend tangent for each side
		#and offset it from the point by the rounding ratio (var offset)
		var tangents := []
		var ends := []
		for side in [[side_a, -1], [side_b, 1]]:
			var inter = points[i] + (side[0].normalized() * offset)
			var tangent = side[0].rotated(deg_to_rad(90 * side[1]))
			tangents.append(tangent)
			ends.append(inter)
		
		#find center of circle used to draw arc
		#by finding the point where the tangents intersect
		var circle_center : Vector2 = Geometry2D.line_intersects_line(
			ends[0], tangents[0], ends[1], tangents[1])
		
		#find out how much of the circle we 'travel', and how quickly
		var radius_a = ends[0] - circle_center
		var radius_b = ends[1] - circle_center
		var circle_angle = rad_to_deg(radius_a.angle_to(radius_b))
		var angle_step = circle_angle / (circle_steps + 1)
		
		#add the points
		final_points.append(ends[0])
		for c in circle_steps:
			radius_a = radius_a.rotated(deg_to_rad(angle_step))
			final_points.append(circle_center + radius_a)
		final_points.append(ends[1])
	
	return final_points
