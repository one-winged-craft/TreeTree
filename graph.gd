class_name Graph


static func empty() -> Array:
	return []


static func vertex(edges := []) -> Array:
	return edges


static func vertices(graph: Array) -> Array:
	return graph


static func add_vertex() -> Callable:
	return func(graph: Array) -> Array:
		return graph + [vertex()]


static func remove_vertex(at: int) -> Callable:
	return func(graph: Array) -> Array:
		return range(graph.size()).reduce(
			func(res: Array, id: int):
				return res if id == at else (
					res + [graph[id].filter(func(e: int) -> bool: return e != at
					).map(func(e: int) -> int: return e if e < at else e - 1)]
				),
			[]
		)


static func add_edge(src: int, dest: int) -> Callable:
	return func(graph: Array) -> Array:
		return Fun.map_index(graph,
			func(id: int, v: Array) -> Array:
				return v + [dest] if id == src else v
		)


static func remove_edge(src: int, dest: int) -> Callable:
	return func(graph: Array) -> Array:
		var new_graph: Array = graph.duplicate()
		if src < new_graph.size() and dest in new_graph[src]:
			new_graph[src] = vertex(
				new_graph[src].filter(func(e: int) -> bool: return e != dest)
			)
		return new_graph


static func with(fs: Array[Callable], graph: Array) -> Array:
	var accumulator := func(g: Array, f: Callable) -> Array:
		return f.call(g)
	return fs.reduce(accumulator, graph)


static func vertex_has_child(graph: Array, src: int, dst: int) -> bool:
	return dst in graph[src]


static func topo(graph: Array) -> Dictionary:
	var sorted: Array = _topo_helper(graph,
		range(graph.size()).filter(func(idx: int) -> bool:
			return graph[idx].is_empty())
	)
	if sorted.size() == graph.size():
		return { ok = sorted }
	return { err = "This graph cannot be sorted."}


static func _topo_helper(graph: Array, e_ids: Array, ord = []) -> Array:
	if e_ids.is_empty(): return ord

	var unlink := func unlink(g: Array, index: int) -> Dictionary:
		return range(g.size()).reduce(
			func(res, i) -> Dictionary:
				if not index in g[i]:
					return {graph = res.graph + [g[i]], sinks = res.sinks}
				var new_edges: Array = g[i].filter(
					func(edge: int) -> bool: return edge != index
				)
				return {
					graph = res.graph + [vertex(new_edges)],
					sinks = res.sinks + ([i] if new_edges.is_empty() else []),
				},
			{graph = [], sinks = []}
		)

	var head: int = e_ids[0]
	var tail: Array = e_ids.slice(1)
	var new: Dictionary = unlink.call(graph, head)
	return _topo_helper(new.graph, tail + new.sinks, [head] + ord)


static func get_roots(graph: Array, order: Array, from := 0, children := []) -> Array:
	if from == order.size():
		return []
	var current: int = order[from]
	var new_children: Array = children + graph[current]
	return ([current] if current not in children else []) + get_roots(graph, order, from + 1, new_children) 
