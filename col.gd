class_name Col


const WHITE = 0
const RED = 1
const YELLOW = 2
const ORANGE = RED ^ YELLOW
const BLUE = 4
const PURPLE = RED ^ BLUE
const GREEN = YELLOW ^ BLUE
const BLACK = RED ^ YELLOW ^ BLUE


const ADD = 0
const SUB = 1
const IS = 2
const ANY = 3


static func get_all() -> Array:
	return range(BLACK + 1)


static func get_all_except(color: int) -> Array:
	return get_all().filter(func(col: int) -> bool: return col != color)


static func get_color(color: int) -> Color:
	match color:
		WHITE: return Color.WHITE
		RED: return Color("ff8080")
		YELLOW: return Color("f5ef82")
		ORANGE: return Color("ffb87a")
		BLUE: return Color("80b7ff")
		PURPLE: return Color("e793ed")
		GREEN: return Color("a4e874")
		BLACK: return Color("828282")
	assert(false, str(color) + " is not a valid color")
	return Color.FUCHSIA # unreachable


static func str(color: int) -> String:
	match color:
		WHITE: return "WHITE"
		RED: return "RED"
		YELLOW: return "YELLOW"
		ORANGE: return "ORANGE"
		BLUE: return "BLUE"
		PURPLE: return "PURPLE"
		GREEN: return "GREEN"
		BLACK: return "BLACK"
	assert(false, str(color) + " is not a valid color")
	return ""


static func add(color_a: int, color_b: int) -> int:
	return color_a | color_b


static func sub(color_a: int, color_b:int) -> int:
	return color_b & ~color_a


static func op(operation: int, color_a: int) -> Callable:
	match operation:
		ADD: return func(color_b: int) -> int: return add(color_a, color_b)
		SUB: return func(color_b: int) -> int: return sub(color_a, color_b)
		IS: return func(color_b: int) -> bool: return color_a == color_b
		ANY: return func(_color_b: int) -> bool: return true
	assert(false, str(operation) + " is not a valid operation")
	return func(i): return i
