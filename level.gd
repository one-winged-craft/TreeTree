class_name Level


# NODE UTILITIES

static func box2(
		color_a: int, color_b: int, modifier_a: Dictionary, modifier_b: Dictionary
	) -> Dictionary:
	return {
		color_a = color_a, color_b = color_b,
		modifier_a = modifier_a, modifier_b = modifier_b
	}


static func with_colors2(
		orb: Dictionary, color_a: int, color_b: int
	) -> Dictionary:
	return box2(color_a, color_b, orb.modifier_a, orb.modifier_b)


static func with_modifiers2(
	orb: Dictionary, modifier_a: Dictionary, modifier_b: Dictionary
	) -> Dictionary:
	return box2(orb.color_a, orb.color_b, modifier_a, modifier_b)


static func set_color_a(orb: Dictionary, col: int) -> Dictionary:
	return box2(col, orb.color_b, orb.modifier_a, orb.modifier_b)


static func set_color_b(orb: Dictionary, col: int) -> Dictionary:
	return box2(orb.color_a, col, orb.modifier_a, orb.modifier_b)


static func set_modifier_a(orb: Dictionary, op: int) -> Dictionary:
	return box2(orb.color_a, orb.color_b, {op = op, col = orb.modifier_a.col}, orb.modifier_b)


static func set_modifier_b(orb: Dictionary, op: int) -> Dictionary:
	return box2(orb.color_a, orb.color_b, orb.modifier_a, {op = op, col = orb.modifier_b.col})


static func set_modifier_color_a(orb: Dictionary, col: int) -> Dictionary:
	return box2(orb.color_a, orb.color_b, {op = orb.modifier_a.op, col = col}, orb.modifier_b)


static func set_modifier_color_b(orb: Dictionary, col: int) -> Dictionary:
	return box2(orb.color_a, orb.color_b, orb.modifier_a, {op = orb.modifier_b.op, col = col})


static func add_colors2(
		orb: Dictionary, color_a: int, color_b: int
	) -> Dictionary:
	return with_colors2(orb,
		merge(orb.color_a, color_a),
		merge(orb.color_b, color_b)
	)


# MODIFIER UTILITIES


static func is_box2(op: int) -> bool:
	return op == Col.ADD or op == Col.SUB


static func mod(op: int, col: int) -> Dictionary:
	return {op = op, col = col}


static func new_color(modifier: Dictionary, color: int) -> int:
	if is_box2(modifier.op):
		return Col.op(modifier.op, modifier.col).call(color)
	return color


static func mod_add(col: int) -> Dictionary:
	return {op = Col.ADD, col = col}


static func mod_sub(col: int) -> Dictionary:
	return {op = Col.SUB, col = col}


static func mod_is(col: int) -> Dictionary:
	return {op = Col.IS, col = col}


static func mod_any(col := Col.WHITE) -> Dictionary:
	return {op = Col.ANY, col = col}


static func merge(color_a: int, color_b: int) -> int:
	return Col.op(Col.ADD, color_a).call(color_b)



# WHOLE LEVEL UTILITIES


const empty := {
	graph = [],
	nodes = [],
}


static func set_node(at: int, node: Dictionary) -> Callable:
	return func(level: Dictionary) -> Dictionary:
		return propagate2({
			graph = level.graph,
			nodes = Fun.map_index(
				level.nodes,
				func(idx: int, old_node: Dictionary) -> Dictionary:
					return node if idx == at else old_node
		)
		}, {order = Graph.topo(level.graph).ok})


static func add_node(color_a := Col.WHITE, color_b := Col.WHITE) -> Callable:
	return func(level: Dictionary) -> Dictionary:
		return {
			graph = Graph.add_vertex().call(level.graph),
			nodes = level.nodes + [
				box2(color_a, color_b, mod_any(), mod_any())
			]
		}


static func remove_node(at: int) -> Callable:
	return func(level: Dictionary) -> Dictionary:
		return {
			graph = Graph.remove_vertex(at).call(level.graph),
			nodes = Fun.remove_at(at, level.nodes)
		}


static func edit_edges_by(graph_op: Callable, src: int, dest: int) -> Callable:
	return func(level: Dictionary) -> Dictionary:
		var graph: Array = graph_op.call(src, dest).call(level.graph)
		var sort: Dictionary = Graph.topo(graph)
		if sort.has("ok"):
			return propagate2(
				{graph = graph, nodes = level.nodes}, 
				{order = sort.ok, idx = 0}
			)
		return level # unmodified level if adding an edge causes loops


static func link(src: int, dest: int) -> Callable:
	return edit_edges_by(Graph.add_edge, src, dest)


static func unlink(src: int, dest: int) -> Callable:
	return edit_edges_by(Graph.remove_edge, src, dest)


static func with(operations: Array, level: Dictionary) -> Dictionary:
	return operations.reduce(func(l, o): return o.call(l), level)


static func switch_modifiers2(at: int) -> Callable:
	return func(level: Dictionary) -> Dictionary:
		var sort := Graph.topo(level.graph)
		if sort.has("err"):
			return level # Return unmodified level if it loops
		var nodes: Array = range(level.nodes.size()).reduce(
			func(ns: Array, id: int) -> Array:
				var orb: Dictionary = level.nodes[id]
				return ns + [
					with_modifiers2(orb, orb.modifier_b, orb.modifier_a) if at == id else orb
				],
			[]
		)
		return propagate2({nodes = nodes, graph = level.graph}, {
			order = sort.ok,
			idx = sort.ok.find(at),
			visited = [],
		})


static func propagate2(level: Dictionary, arguments: Dictionary) -> Dictionary:
	var signature := {
		order = "the `order' in which we operate on the graph",
		idx = {desc = "the `idx' of the current visited vertex on the graph", default = 0},
		visited = {desc = "the list of vertices we already have `visited'", default = []},
	}
	var args := Fun.arguments_defaults(signature, arguments)
	assert(Fun.arguments(signature, args))

	if args.idx == args.order.size(): return level
	var current: int = args.order[args.idx]
	var parent: Dictionary = level.nodes[current]
	var color_a: int = new_color(parent.modifier_a, parent.color_a)
	var color_b: int = new_color(parent.modifier_b, parent.color_b)
	var result: Dictionary = range(level.graph.size()).reduce(
		func(res: Dictionary, id: int) -> Dictionary:
			var child: Dictionary = level.nodes[id]
			if id == current or not id in level.graph[current]: # vertex is self or not a child
				return {nodes = res.nodes + [child], visited = res.visited}
			if child.color_a == color_a and child.color_b == color_b: # child is not changed but visited
				return {nodes = res.nodes + [child], visited = res.visited + [id]}
			return { # child is changed and visited
				nodes = res.nodes + [
					add_colors2(child, color_a, color_b) if id in res.visited # add if child has already been visited
					else with_colors2(child, color_a, color_b) # assign if it's the first visit in rules propagation
				],
				visited = args.visited + [id]
			},
		{nodes = [], visited = args.visited + [current]}
	)
	return propagate2({
		nodes = result.nodes,
		graph = level.graph,
	}, {
		order = args.order,
		idx = args.idx + 1,
		visited = result.visited,
	})
