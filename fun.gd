class_name Fun
extends Object



static func arguments(reqs: Dictionary, args: Dictionary) -> bool:
	var success: bool = reqs.keys().map(
		func(req: String) -> bool:
			if not args.has(req):
				push_error("Missing `" + req + "' argument.")
			args[req] = args[req] if args.has(req) else reqs[req].default
			return args.has(req)
	).all(func(b: bool) -> bool: return b)
	if not args.size() == reqs.size():
		push_error(
			"Extraneous argument " + args.keys()[
				Fun.find_index_by(
					args.keys(),
					func(arg: String) -> bool: return arg in reqs.keys()
				)
			] + "."
		)
	return success and args.size() == reqs.size()


static func arguments_defaults(reqs: Dictionary, args: Dictionary) -> Dictionary:
	return reqs.keys().reduce(
		func(res: Dictionary, req: String) -> Dictionary:
			if args.has(req):
				return dictionary_merge(res, {req: args[req]})
			return dictionary_merge(res, {req: reqs[req].default}),
		{},
	)


static func repeat(n: int, x) -> Array: # Nothing to hide
	var result := []
	result.resize(n)
	result.fill(x)
	return result


static func array_flatten(xxs: Array[Array]) -> Array:
	return xxs.reduce(
		func(result: Array, xs: Array) -> Array:
			return result + xs,
		[]
	)


static func dictionary_merge(a: Dictionary, b: Dictionary, deep := true) -> Dictionary:
	var result := a.duplicate(deep)
	result.merge(b.duplicate(deep))
	return result


static func remove_at(at: int, xs: Array, deep = true) -> Array:
	var result := xs.duplicate(deep)
	result.remove_at(at)
	return result


static func map_index(xs: Array, f: Callable) -> Array:
	return range(xs.size()).reduce(
		func(res: Array, cur: int) -> Array:
			return res + [f.call(cur, xs[cur])],
		[]
	)


static func find_index_by(xs: Array, f: Callable, from := 0) -> int:
	if xs.is_empty():
		return -1
	return from if f.call(xs[from]) else find_index_by(xs, f, from + 1)


static func find_by(xs: Array, f: Callable, from := 0) -> Array:
	var index := find_index_by(xs, f, from)
	return [] if index == -1 else [xs[index]]
