@tool
class_name DrawSimple


static func draw_edge(ctx: CanvasItem, from: Vector2, to: Vector2, edge_width: int):
	ctx.draw_polyline_colors(
		[to, from + (to - from) / 2, from],
		[Color.WHITE, Color.BLACK],
		edge_width
	)


static func draw_node(ctx: CanvasItem, origin: Vector2, size: int, _node: Dictionary):
	ctx.draw_circle(origin, size / 2, Color.BLACK)
