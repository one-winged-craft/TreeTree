@tool
class_name Context
extends Control


enum DrawContext {DRAW_LEVEL, EDIT_NODE, EDIT_DONE}
enum Action {NO_ACTION, DELETE_GRAPH}

const level_rect_color := Color(1.0, 1.0, 1.0, 0.02)

const graph_size := 150.0
const scale_factor_increment := graph_size * 0.1
const scale_factor_min := graph_size * 0.6
const scale_factor_max := graph_size * 1.4

var scale_factor := graph_size:
	set(factor):
		scale_factor = clampf(factor, scale_factor_min, scale_factor_max)
		edge_width = scale_factor / 30.0
		vertex_size = scale_factor / 3.0
var edge_width := scale_factor / 30.0
var vertex_size := scale_factor / 3.0
var offset := Vector2.ZERO

var graph_drawing := DrawDebug
var graph_embedding := GraphDrawingFree

var overlay_drawing := OverlayCircle
var overlay_node: Dictionary = Level.box2(Col.WHITE, Col.WHITE, Level.mod_any(), Level.mod_any())
var overlay_already_opened := false

var selected_vertex := {err = "no_vertex"}
var mouse_following_vertex := {err = "no_vertex"}
var context := DrawContext.DRAW_LEVEL

var operations: Array[Array] = []

var initial_level := Level.empty
var current_level := Level.empty
var current_topo := []
var vertices := []
var metas := {}

@onready var font := ThemeDB.fallback_font
@onready var font_size := ThemeDB.fallback_font_size



func add_operation(op: Array) -> void:
	operations.append(op)
	current_level = Level.with(op, current_level)
	current_topo = Graph.topo(current_level.graph).ok


func get_operations() -> Array:
	return Fun.array_flatten(operations)


static func update_metas_positions(metas: Dictionary, positions: Array) -> Dictionary:
	var new_metas := metas.duplicate()
	new_metas.positions = positions
	return new_metas


func update(state: Dictionary, draw_area: Rect2) -> void:
	var roots := Graph.get_roots(current_level.graph, current_topo)

	var position: Vector2 = state.position
	var edit_node := func(vertex: int, already_opened: bool) -> Array:
		var new := overlay_drawing.edit_node({
			node = overlay_node,
			position = state.position - (vertices[vertex] + offset) * scale_factor,
			pressed = state.pressed,
			vertex_size = vertex_size,
			already_opened = already_opened,
			is_root = selected_vertex.ok in roots,
		})
		context = DrawContext.EDIT_NODE
		overlay_node = new.node if new.has("node") else overlay_node
		if new.done:
			context = DrawContext.EDIT_DONE
			return [Level.set_node(selected_vertex.ok, new.node)]
		return []

	var set_scale_factor := func(factor: float) -> void:
		var minimal_rect := get_minimal_rect(metas.positions)
		var point := position / scale_factor - minimal_rect.get_center()
		scale_factor += factor
		var point_offset := point - (position / scale_factor - minimal_rect.get_center())
		offset = clamp_offset(offset - point_offset, scale_factor, draw_area, minimal_rect, vertex_size)
		metas = update_metas_positions(metas, center_metas_positions(metas.positions))
		queue_redraw()

	match [context, state]:
		[_, {"button": MOUSE_BUTTON_WHEEL_UP, ..}]:
			set_scale_factor.call(scale_factor_increment)
		[_, {"button": MOUSE_BUTTON_WHEEL_DOWN, ..}]:
			set_scale_factor.call(- scale_factor_increment)
		[_, {"button": MOUSE_BUTTON_MIDDLE, "pressed": true, ..}]:
			mouse_following_vertex = select_vertex(position, offset, vertex_size, scale_factor, vertices, draw_area)
		[_, {"button": MOUSE_BUTTON_MIDDLE, "pressed": false, ..}]:
			var current_vertex := select_vertex(position, offset, vertex_size, scale_factor, vertices, draw_area)
			match [mouse_following_vertex, current_vertex]:
				[{"ok": var id, ..}, {"err": "no_vertex", ..}]:
					var new_metas := metas.duplicate()
					new_metas.positions[id] = position / scale_factor - offset
					metas = new_metas
					queue_redraw()
				[{"err": "no_vertex", "position": var start}, _]:
					metas = update_metas_positions(metas, center_metas_positions(metas.positions))
					var minimal_rect := get_minimal_rect(metas.positions)
					offset = minimal_rect.get_center() + clamp_offset(offset + start - position / scale_factor, scale_factor, draw_area, minimal_rect, vertex_size)
					queue_redraw()
			mouse_following_vertex = {err = "no_vertex"}
		[DrawContext.DRAW_LEVEL, {"button": MOUSE_BUTTON_LEFT, "pressed": true, ..}]:
			selected_vertex = select_vertex(position, offset, vertex_size, scale_factor, vertices, draw_area)
		[DrawContext.DRAW_LEVEL, {"button": MOUSE_BUTTON_LEFT, "pressed": false, ..}]:
			var updated := draw_level({
				current_vertex = select_vertex(position, offset, vertex_size, scale_factor, vertices, draw_area),
				previous_vertex = selected_vertex,
				level = current_level,
				position = position / scale_factor - offset,
				metas = metas,
			})
			add_operation(updated.operations)
			metas = updated.metas
			selected_vertex = {err = "no_vertex"}
			queue_redraw()

		[DrawContext.DRAW_LEVEL, {"button": MOUSE_BUTTON_RIGHT, "pressed": true , ..}]:
			overlay_already_opened = false
			selected_vertex = select_vertex(position, offset, vertex_size, scale_factor, vertices, draw_area)
			if is_valid_vertex(selected_vertex, current_level.graph):
				overlay_node = current_level.nodes[selected_vertex.ok]
				add_operation(edit_node.call(selected_vertex.ok, overlay_already_opened))
				queue_redraw()
		[DrawContext.EDIT_DONE, {"button": MOUSE_BUTTON_RIGHT, "pressed": true, ..}]:
			overlay_already_opened = false
			selected_vertex = select_vertex(position, offset, vertex_size, scale_factor, vertices, draw_area)
			if is_valid_vertex(selected_vertex, current_level.graph):
				overlay_node = current_level.nodes[selected_vertex.ok]
				add_operation(edit_node.call(selected_vertex.ok, overlay_already_opened))
				queue_redraw()

		[DrawContext.EDIT_NODE, {"pressed": true, ..}]:
			add_operation(edit_node.call(selected_vertex.ok, overlay_already_opened))
			queue_redraw()
		[DrawContext.EDIT_NODE, {"pressed": false, ..}]:
			overlay_already_opened = Geometry2D.is_point_in_circle(position, (vertices[selected_vertex.ok] + offset) * scale_factor, vertex_size / 2)
			add_operation(edit_node.call(selected_vertex.ok, overlay_already_opened))
			queue_redraw()

		[DrawContext.EDIT_DONE, {"button": MOUSE_BUTTON_LEFT, ..}]:
			selected_vertex = select_vertex(position, offset, vertex_size, scale_factor, vertices, draw_area)
			context = DrawContext.DRAW_LEVEL


func keyboard_update(state: Dictionary, draw_area: Rect2) -> void:
	match state:
		{"action": Action.DELETE_GRAPH, ..}:
			initial_level = Level.empty
			operations = []
			queue_redraw()


static func is_valid_vertex(vertex: Dictionary, graph: Array) -> bool:
	return vertex.has("ok") and vertex.ok < graph.size()


static func select_vertex(point: Vector2, offset: Vector2, size: float, scale: float, embedding: Array, area: Rect2) -> Dictionary:
	if not area.has_point(point):
		return {err = "outside_context"}
	for id in range(embedding.size()):
		if Geometry2D.is_point_in_circle(point, (embedding[id] + offset) * scale, size / 2):
			return {ok = id}
	return {err = "no_vertex", position = point / scale}


static func draw_level(args: Dictionary) -> Dictionary:
	assert(Fun.arguments({
		current_vertex = "`current_vertex', the currently selected vertex",
		previous_vertex = "`previous_vertex', the previously selected vertex",
		level = "current `level' of the editor",
		position = "current mouse `position'",
		metas = "`metas', level-assosciated dictionary of metadatas"
	}, args))

	var add_current_position := func(position: Vector2 = args.position) -> Dictionary:
		var new_metas: Dictionary = args.metas.duplicate()
		new_metas.positions = (args.metas.positions if args.metas.has("positions") else []) + [position]
		return new_metas

	var remove_current_position := func(id: int) -> Dictionary:
		var new_metas: Dictionary = args.metas.duplicate()
		var new_positions: Array = args.metas.positions.duplicate()
		new_positions.remove_at(id)
		new_metas.positions = new_positions
		return new_metas

	match [args.previous_vertex, args.current_vertex]:
		[{"err": "no_vertex", ..}, {"err": "no_vertex", ..}]:
			return {operations = [Level.add_node()] as Array, metas = add_current_position.call()}
		[{"err": "no_vertex", ..}, {"err": "outside_context", ..}]:
			var remove_all: Array
			for id in range(args.level.graph.size() - 1, -1, -1):
				remove_all.append(Level.remove_node(id))
			var new_metas : Dictionary = args.metas.duplicate()
			new_metas.positions = []
			return {operations = remove_all, metas = new_metas}
		[{"ok": var id, ..}, {"err": "no_vertex", ..}]:
			return {
				operations = [Level.add_node(), Level.link(id, args.level.graph.size())] as Array,
				metas = add_current_position.call()
			}
		[{"ok": var id, ..}, {"err": "outside_context", ..}]:
			return {operations = [Level.remove_node(id)] as Array, metas = remove_current_position.call(id)}
		[{"err": "no_vertex", "position": var position, ..}, {"ok": var id, ..}]:
			return {
				operations = [Level.add_node(), Level.link(args.level.graph.size(), id)] as Array,
				metas = add_current_position.call(position)
			}
		[{"ok": var from}, {"ok": var to}]:
			if from == to:
				return {operations = [Level.switch_modifiers2(to)] as Array, metas = args.metas}
			return {
				operations = toggle_link({from = from, to = to, level = args.level}),
				metas = args.metas
			}
	return {operations = [], metas = args.metas}


static func toggle_link(args: Dictionary) -> Array:
	assert(Fun.arguments({
		level = "`level' on which we should (un)link nodes",
		from = "node to add or remove an edge `from'",
		to = "node to add or remove an edge `to'",
	}, args))

	if args.to in args.level.graph[args.from]:
		return [Level.unlink(args.from, args.to)] as Array
	return [Level.link(args.from, args.to)] as Array


func _draw() -> void:
	var roots: Array = Graph.get_roots(current_level.graph, current_topo)
	var embedding = graph_embedding.draw(current_level.graph, metas)
	vertices = embedding.vertices
	metas = embedding.metas

	draw_rect(level_rect_to_screen(offset, scale_factor, get_minimal_rect(vertices)), level_rect_color, true)

	for id in current_topo:
		var position: Vector2 = (vertices[id] + offset) * scale_factor
		for edge in current_level.graph[id]:
			var edge_position: Vector2 = (vertices[edge] + offset) * scale_factor
			graph_drawing.draw_edge(self, position, edge_position, edge_width)
		graph_drawing.draw_node(self, position, vertex_size, current_level.nodes[id])
		graph_embedding.draw_label(self, font, vertex_size / 3, position, str(current_topo.find(id)) + ("r" if id in roots else ""))

	if context == DrawContext.EDIT_NODE:
		overlay_drawing.draw({
			ctx = self,
			vertex_position = (vertices[selected_vertex.ok] + offset) * scale_factor,
			vertex_size = vertex_size,
			edge_width = edge_width,
			node = overlay_node,
			draw_node = graph_drawing.draw_node,
			is_root = selected_vertex.ok in roots,
		})


static func clamp_offset(offset: Vector2, scale_factor: float, screen: Rect2, level: Rect2, vertex_size: float) -> Vector2:
	var level_margin := Vector2.ONE * vertex_size
	return offset.clamp(
		(screen.position + level_margin) / scale_factor - level.size / 2,
		(screen.end - level_margin) / scale_factor + level.size / 2
	)


static func center_metas_positions(positions: Array) -> Array:
	return positions.map(
		func(pos: Vector2) -> Vector2:
			return pos - get_minimal_rect(positions).get_center()
	)


static func level_rect_to_screen(offset: Vector2, scale_factor: float, level: Rect2) -> Rect2:
	return Rect2((level.position + offset) * scale_factor, level.size * scale_factor)


static func get_minimal_rect(positions: Array) -> Rect2:
	return positions.reduce(
		func(rect: Rect2, pos: Vector2) -> Rect2:
			var start := Vector2(minf(rect.position.x, pos.x), minf(rect.position.y, pos.y))
			var end := Vector2(maxf(rect.end.x, pos.x), maxf(rect.end.y, pos.y))
			return Rect2(start, end - start),
		Rect2(INF, INF, -INF, -INF)
	)


func to_serialize() -> Dictionary:
	var serialized_positions: Array = (metas.positions if metas.has("positions") else []).map(
		func(v: Vector2) -> Dictionary: return {x = v.x, y = v.y}
	)
	var new_metas := metas.duplicate()
	new_metas.positions = serialized_positions
	return {
		level = Level.with(get_operations(), initial_level),
		metas = new_metas
	}


func from_serialize(state := {level = Level.empty, metas = {}}):
	var new_metas: Dictionary = state.metas.duplicate()
	new_metas.positions = (
		state.metas.positions if state.metas.has("positions") else []
	).map(
		func(d: Dictionary) -> Vector2: return Vector2(d.x, d.y)
	)
	initial_level = state.level
	current_level = state.level
	current_topo = Graph.topo(current_level.graph).ok
	var embedding: Dictionary = graph_embedding.draw(initial_level.graph, new_metas)
	vertices = embedding.vertices
	metas = embedding.metas
	operations = []
	queue_redraw()
