@tool
class_name LevelControl
extends Control


@onready var context := $Context
var state := {level = Level.empty, metas = {}}


func _ready():
	context.from_serialize(state)


func _gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		context.update({
			pressed = event.pressed,
			button = event.button_index,
			position = context.get_local_mouse_position(),
		}, Rect2(self.position - self.size / 2, self.size))
	elif event is InputEventKey:
		context.keyboard_update({
			pressed = event.pressed,
			action = keycode_to_action(event.keycode),
			position = context.get_local_mouse_position(),
		}, Rect2(self.position - self.size / 2, self.size))


func keycode_to_action(keycode: int) -> int:
	match keycode:
		KEY_BACKSPACE: return Context.Action.DELETE_GRAPH
		_: return Context.Action.NO_ACTION


func to_serialize() -> Dictionary:
	return context.to_serialize()


func from_serialize(state: Dictionary):
	self.state = state
