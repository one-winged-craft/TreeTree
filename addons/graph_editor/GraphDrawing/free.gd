class_name GraphDrawingFree
extends Object


static func draw(_graph: Array, metas: Dictionary) -> Dictionary:
	return {
		vertices = metas.positions if metas.has("positions") else [],
		metas = metas,
	}


static func draw_label(ctx: CanvasItem, font: Font, font_size: int, vertex: Vector2, label: String) -> void:
	ctx.draw_string(font, vertex + Vector2(1, -1) * 1.2 * font_size, label, HORIZONTAL_ALIGNMENT_CENTER, -1, font_size, Color.BLACK)
