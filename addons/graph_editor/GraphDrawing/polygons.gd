class_name GraphDrawingPolygons
extends Object


static func draw(graph: Array, metas: Dictionary) -> Dictionary:
	var n: int = graph.size()
	return {
		vertices = range(n).map(
			func(i: int) -> Vector2:
				var ang := TAU * i / n
				return Vector2(cos(ang), sin(ang))),
		metas = metas,
	}


static func draw_label(ctx: CanvasItem, font: Font, font_size: int, vertex: Vector2, label: String) -> void:
	font.draw_string(ctx.get_canvas_item(), vertex + Vector2(
		-1.8 if vertex.x < 0 else 1, -1 if vertex.y < 0 else 1.8
	) * 1.2 * font_size, label, HORIZONTAL_ALIGNMENT_CENTER, -1, font_size, Color.BLACK)
