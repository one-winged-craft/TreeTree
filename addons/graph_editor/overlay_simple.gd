@tool
class_name OverlaySimple


static func draw(args: Dictionary):
	assert(Fun.arguments({
		node = "The currently selected `node': Dictionary",
		position = "The mouse `position' relative from the vertex center: Vector2",
		pressed = "Whether the mouse is `pressed' or released at current position",
		vertex_size = "The `vertex_size' in pixels: int",
		already_opened = "Whether the edit node was `already_opened' or not: bool",
	}, args))


static func edit_node(args: Dictionary) -> Dictionary:
	assert(Fun.arguments({
		node = "The currently selected `node': Dictionary",
		position = "The mouse `position' relative from the vertex center: Vector2",
		pressed = "Whether the mouse is `pressed' or released at current position: bool",
		vertex_size = "The `vertexe_size' in pixels: int",
		already_opened = "Was the overlay `already_opened' before: bool",
	}, args))
	var rand := RandomNumberGenerator.new()
	rand.randomize()
	var is_modifying_color := (rand.randi() % 2) * 2
	var random_col := func() -> int: return rand.randi() % Col.BLACK
	var random_mod := func() -> Dictionary:
		return {op = (rand.randi() % 2) + is_modifying_color, col = random_col.call()}
	return { done = false } if args.pressed else  {
		done = true,
		node = Level.box2(
			random_col.call(), random_col.call(),
			random_mod.call(), random_mod.call()
		),
	}
