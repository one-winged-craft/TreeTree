@tool
class_name OverlayCircle


enum Selection {VERTEX, OVERLAY_MOD, OVERLAY_COLOR, OUTSIDE}
enum NodeType {BOX, ORB}


const overlay_factor := 2.2


static func overlay_size(vertex_size: int) -> float:
	return vertex_size / 2 * overlay_factor


static func overlay_mod_size(vertex_size: int) -> float:
	return (overlay_size(vertex_size) - vertex_size / 2) / 2 + (vertex_size / 2) 


static func pie_portion(pie_index: int, pie_count: int, cc: bool, precision := 3) -> Array:
	return [Vector2.ZERO] + range(precision + 1).map(
		func(n: float) -> Vector2:
			return Vector2.RIGHT.rotated(
				PI / pie_count * (pie_index + n / precision)) * (1 if cc else -1)
			)


static func draw(args: Dictionary):
	assert(Fun.arguments({
		ctx = "The `ctx' to draw on: CanvasLayer",
		node = "The currently selected `node': Dictionary",
		edge_width = "The `edge_width' in pixels: int",
		vertex_position = "The vertex `position': Vector2",
		vertex_size = "The `vertex_size' in pixels: int",
		draw_node = "The callback in order to `draw_node': Callable",
		is_root = "The current node is a root of the graph : bool",
	}, args))

	args.ctx.draw_set_transform(args.vertex_position)
	draw_half_colors(args.ctx, args.node.color_a if args.is_root else args.node.modifier_a.col, false, args.vertex_size, args.edge_width)
	draw_half_colors(args.ctx, args.node.color_b if args.is_root else args.node.modifier_b.col, true, args.vertex_size, args.edge_width)
	if not args.is_root:
		args.ctx.draw_circle(Vector2.ZERO, overlay_mod_size(args.vertex_size), Color.BLACK)
	args.draw_node.call(args.ctx, args.vertex_position, args.vertex_size, args.node)
	args.ctx.draw_set_transform(Vector2.ZERO)


static func draw_half_colors(ctx, color: int, cc : bool, vertex_size: int, width: int) -> void:
	var colors := Col.get_all_except(color)
	if cc: colors.reverse()
	for index in colors.size():
		var pie := pie_portion(index, colors.size(), cc).map(
			func(v: Vector2) -> Vector2: return v * overlay_size(vertex_size)
		)
		ctx.draw_colored_polygon(pie, Col.get_color(colors[index]))
		ctx.draw_polyline(pie + [pie[0]], Color.BLACK, width / 4, true)



static func edit_node(args: Dictionary) -> Dictionary:
	assert(Fun.arguments({
		node = "The currently selected `node': Dictionary",
		position = "The mouse `position' relative from the vertex center: Vector2",
		pressed = "Whether the mouse is `pressed' or released at current position: bool",
		vertex_size = "The `vertex_size' in pixels: int",
		already_opened = "Whether the edit node was `already_opened' or not: bool",
		is_root = "The current node is a root of the graph : bool",
	}, args))

	if args.is_root and args.node.modifier_a.op != Col.ANY and args.node.modifier_b.op != Col.ANY:
		return {done = false, node = node_set_type(args.node, NodeType.ORB)}

	var set_node_any_and_color := func(node: Dictionary) -> Dictionary:
		return set_node_half_color(
			args.position,
			set_node_half_any_to_is(args.node, args.position, args.is_root),
			args.is_root
		)

	match {
		already_opened = args.already_opened,
		pressed = args.pressed,
		selection = current_selection(args.position, args.vertex_size, args.is_root),
	}:
		{"already_opened": false, "pressed": true, "selection": Selection.VERTEX}:
			return {done = false}
		{"already_opened": true, "pressed": false, "selection": Selection.VERTEX}:
			return {done = false}
		{"already_opened": true, "pressed": true, "selection": Selection.VERTEX}:
			return {done = false, node = args.node if args.is_root else node_toggle_type_on_a(args.node)}
		{"already_opened": true, "pressed": false, "selection": Selection.OVERLAY_MOD}:
			return {done = false, node = set_node_half_modifier(args.position, args.node)}
		{"already_opened": false, "pressed": false, "selection": Selection.OVERLAY_MOD}:
			return {done = true, node = set_node_half_modifier(args.position, args.node)}
		{"already_opened": true, "pressed": var pressed, "selection": Selection.OVERLAY_MOD}:
			return {done = false}
		{"already_opened": true, "pressed": false, "selection": Selection.OVERLAY_COLOR}:
			return {done = false, node = set_node_any_and_color.call(args.node)}
		{"already_opened": false, "pressed": false, "selection": Selection.OVERLAY_COLOR}:
			return {done = true, node = set_node_any_and_color.call(args.node)}
		{"already_opened": true, "pressed": var pressed, "selection": Selection.OVERLAY_COLOR}:
			return {done = false}
	return {
		done = true,
		node = args.node,
	}


static func set_node_half_any_to_is(node: Dictionary, position: Vector2, is_root: bool) -> Dictionary:
	if is_root: return node
	var is_upper_half := position.y < 0
	var op: int = node.modifier_a.op if is_upper_half else node.modifier_b.op
	var setter := Callable(Level, "set_modifier_a" if is_upper_half else "set_modifier_b")
	
	return setter.call(node, Col.IS) if op == Col.ANY else node


static func set_node_half_modifier(position: Vector2, node: Dictionary) -> Dictionary:
	var cc := -1 if position.y < 0 else 1
	var modifiers := (
		[Col.ADD, Col.SUB] if Level.is_box2(node.modifier_a.op) else [Col.IS, Col.ANY]
	).filter(func(m: int) -> bool: return m != (
		node.modifier_b.op if cc == 1 else node.modifier_a.op
	))
	var modifier: int = modifiers[node_half(position, modifiers.size(), cc)]
	return Level.set_modifier_a(node, modifier) if cc == -1 else Level.set_modifier_b(node, modifier)


static func set_node_half_color(position: Vector2, node: Dictionary, is_root: bool) -> Dictionary:
	var cc := -1 if position.y < 0 else 1
	var result := {
		color_a = node.color_a if is_root else node.modifier_a.col,
		color_b = node.color_b if is_root else node.modifier_b.col,
		setter_a = Callable(Level, "set_color_a" if is_root else "set_modifier_color_a"),
		setter_b = Callable(Level, "set_color_b" if is_root else "set_modifier_color_b"),
	}
	var colors := Col.get_all_except(result.color_a if cc == -1 else result.color_b)
	var color: int = colors[node_half(position, colors.size(), cc)]
	return result.get("setter_a" if cc == -1 else "setter_b").call(node, color) 


static func node_half(position: Vector2, portions: int, cc: int) -> int:
	var angle := position.angle_to(Vector2.RIGHT if cc == -1 else Vector2.LEFT)
	var pie_index : int = floor(angle / PI * float(portions))
	return (portions - pie_index) - 1 if cc == -1 else pie_index


static func current_selection(position: Vector2, vertex_size: int, is_root: bool) -> int:
	if Geometry2D.is_point_in_circle(position, Vector2.ZERO, vertex_size / 2):
		return Selection.VERTEX
	var overlay_size := overlay_size(vertex_size)
	var overlay_mod_size := overlay_mod_size(vertex_size)
	if not is_root and Geometry2D.is_point_in_circle(position, Vector2.ZERO, overlay_mod_size):
		return Selection.OVERLAY_MOD
	if Geometry2D.is_point_in_circle(position, Vector2.ZERO, overlay_size):
		return Selection.OVERLAY_COLOR
	return Selection.OUTSIDE



# NODE MANIPULATION HELPERS


static func node_set_type(node: Dictionary, type: int) -> Dictionary:
	match type:
		NodeType.BOX:
			return Level.with_modifiers2(node, 
				Level.mod_add(node.modifier_a.col),
				Level.mod_add(node.modifier_b.col)
			)
		NodeType.ORB:
			return Level.with_modifiers2(node,
				Level.mod_any(node.modifier_a.col),
				Level.mod_any(node.modifier_b.col)
			)
	return node


static func node_toggle_type_on_a(node: Dictionary) -> Dictionary:
	return node_set_type(node, NodeType.ORB if Level.is_box2(node.modifier_a.op) else NodeType.BOX)
