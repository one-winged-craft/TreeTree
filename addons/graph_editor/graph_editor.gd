@tool
extends EditorPlugin


const save_path := "res://saves.json"
const default_level_name := "DefaultLevel"


var screen := Control.new()
var level_selector := preload("res://addons/graph_editor/level_selector.tscn").instantiate()
var level_selector_button_scene := preload("res://addons/graph_editor/level_selector_button.tscn")
var level_selector_input := level_selector.get_node("VBoxContainer/Create")
var level_scene := preload("res://addons/graph_editor/level.tscn")
var levels := []
var current_level: int


var accent_color := get_editor_interface().get_editor_settings() \
	.get_setting("interface/theme/accent_color")
var delete_color := get_editor_interface().get_editor_settings() \
	.get_setting("text_editor/theme/highlighting/comment_markers/critical_color")


func _input(event):
	if event.is_action_pressed("ui_page_up"):
		for l in levels:
			print(l.id, " ")


func _enter_tree():
	_make_visible(false)

	var level_root := Control.new()
	var level_name := Label.new()
	level_root.name = "Levels"
	level_name.name = "CurrentLevel"

	level_name.set_anchors_preset(Control.PRESET_CENTER_TOP)
	level_name.grow_horizontal = Control.GROW_DIRECTION_BOTH
	for control in [level_root, screen]:
		control.set_anchors_preset(Control.PRESET_FULL_RECT)
		control.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		control.size_flags_vertical = Control.SIZE_EXPAND_FILL

	screen.add_child(level_root)
	screen.add_child(level_selector)
	screen.add_child(level_name)
	EditorInterface.get_editor_main_screen().add_child(screen)

	level_selector_input.connect("text_submitted", Callable(self, "create_level"))

	var loaded_levels = JSON.parse_string(
		FileAccess.open(save_path, FileAccess.READ).get_as_text()
	)
	if loaded_levels == null:
		return

	var to_edges := func(es: Array) -> Array:
		return es.map(func(e: int) -> int: return e)
	var to_node := func(n: Dictionary) -> Dictionary:
		return {
			color_a = int(n.color_a), color_b = int(n.color_a),
			modifier_a = {col = int(n.modifier_a.col), op = int(n.modifier_a.op)},
			modifier_b = {col = int(n.modifier_b.col), op = int(n.modifier_b.op)},
		}
	for state in loaded_levels:
		var level := {
			graph = state.game.level.graph.map(to_edges),
			nodes = state.game.level.nodes.map(to_node),
		}
		create_level(state.name, {level = level, metas = state.game.metas})
	
	if levels.is_empty():
		create_level(default_level_name)


func _exit_tree():
	for level in levels:
		level.scene.queue_free()
	level_selector.queue_free()
	screen.queue_free()


func _has_main_screen():
	return true


func _make_visible(visible: bool) -> void:
	screen.visible = visible


func _get_plugin_name():
	return "Graph Editor"


func _get_plugin_icon():
	return EditorInterface.get_editor_theme().get_icon("Path2D", "EditorIcons")


func _save_external_data():
	var data := levels.map(
		func(level: Dictionary) -> Dictionary:
			return {
				name = level.name,
				game = level.scene.to_serialize()
			}
	)
	FileAccess.open(save_path, FileAccess.WRITE).store_string(
		JSON.stringify(data)
	)


func switch_level(id: int) -> void:
	var index := Fun.find_index_by(levels, func(level: Dictionary) -> bool: return level.id == id)
	if index == -1: return
	current_level = id
	screen.get_node("CurrentLevel").text = levels[index].name
	for level in screen.get_node("Levels").get_children():
		screen.get_node("Levels").remove_child(level)
	screen.get_node("Levels").add_child(levels[index].scene)


func delete_level(id: int, button: Control) -> void:
	var index := Fun.find_index_by(levels, func(level: Dictionary) -> bool: return level.id == id)
	if index == -1: return
	levels[index].scene.queue_free()
	levels.remove_at(index)
	button.queue_free()
	if levels.is_empty():
		create_level(default_level_name)
	if id == current_level:
		switch_level(levels.back().id)


func create_level(level_name: String, state := {level = Level.empty, metas = {positions = []}}) -> void:
	if level_name.is_empty(): return
	
	level_selector_input.text = ""

	var scene := level_scene.instantiate()
	var id := scene.get_instance_id()
	levels.append({
		id = id,
		name = level_name,
		scene = scene
	})
	scene.from_serialize(state)

	switch_level(id)

	var button := level_selector_button_scene.instantiate()
	var select := button.get_node("Select")
	var delete := button.get_node("Delete")
	select.text = level_name
	select.connect("pressed", func() -> void: return switch_level(id))
	select.add_theme_color_override("font_color", accent_color)
	select.add_theme_color_override("font_hover_color", accent_color)

	delete.add_theme_color_override("font_color", delete_color)
	delete.add_theme_color_override("font_hover_color", delete_color)
	delete.connect(
		"pressed", func() -> void: return delete_level(id, button)
	)

	level_selector.get_node("VBoxContainer/ScrollContainer/VBoxContainer").add_child(button)
