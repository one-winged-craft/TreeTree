@tool
class_name DrawDebug


const sprites := {
	orb_main = preload("res://Assets/TestShapes/orb_main.png"),
	orb_inner = preload("res://Assets/TestShapes/orb_inner.png"),
	modifier = preload("res://Assets/TestShapes/modifier.png"),
	plus = preload("res://Assets/TestShapes/plus.png"),
	minus = preload("res://Assets/TestShapes/minus.png")
}


static func draw_edge(ctx: CanvasItem, from: Vector2, to: Vector2, edge_width: int):
	DrawSimple.draw_edge(ctx, from, to, edge_width)


static func draw_node(ctx: CanvasItem, origin: Vector2, size: int, node: Dictionary):
	var ratio := size / float(sprites.orb_main.get_width())
	ctx.draw_set_transform(origin, 0.0) # draw higher half
	draw_half(ctx, ratio, node.color_a, node.modifier_a)
	ctx.draw_set_transform(origin, PI) # draw lower half
	draw_half(ctx, ratio, node.color_b, node.modifier_b)
	ctx.draw_set_transform(Vector2.ZERO, 0.0) # return to canvas origin


static func draw_half(ctx: CanvasItem, size: float, col: int, mod: Dictionary):
	var color := Col.get_color(mod.col)
	match mod.op:
		Col.ADD:
			draw_base(ctx, sprites.modifier, size, color)
			draw_mod_sign(ctx, sprites.plus, size)
		Col.SUB:
			draw_base(ctx, sprites.modifier, size, color)
			draw_mod_sign(ctx, sprites.minus, size)
		Col.IS:
			draw_base(ctx, sprites.orb_main, size, color)
			draw_base(ctx, sprites.orb_inner, size, Col.get_color(col))
		Col.ANY:
			draw_base(ctx, sprites.orb_main, size, Col.get_color(col))


static func draw_base(ctx: CanvasItem, texture: Texture2D, size: float, color := Color(1, 1, 1, 1)):
	ctx.draw_texture_rect(texture, get_sprite_rect(texture, size, Vector2(0.5, 1)), false, color)


static func draw_mod_sign(ctx: CanvasItem, sprite: Texture2D, size: float):
	var surface := get_sprite_rect(sprite, size, Vector2(0.5, 1.14))
	ctx.draw_texture_rect(sprite, surface, false)


static func get_sprite_rect(sprite: Texture2D, size: float, center: Vector2) -> Rect2:
	var sprite_size := sprite.get_size() * size
	return Rect2(- sprite_size * center, sprite_size)
