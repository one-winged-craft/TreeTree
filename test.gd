@tool
extends EditorScript
class_name Test


func col_eq(got: int, want: int) -> bool:
	var success := want == got
	if not success:
		push_error(
			"Color is " + Col.str(got) +
			", but should be " + Col.str(want) + "."
		)
	return success


func _run():
	var start := Time.get_ticks_usec()


	# `FUN`CTIONAL UTILITIES ;)
	assert(Fun.repeat(0, {}).is_empty(), "Not repeating should be empty.")
	assert(Fun.repeat(1, 123) == [123], "Repeating once is one element.")
	assert(Fun.repeat(2, 123) == [123, 123], "Repeating twice is two elements.")

	# LEVELLEVEL IS THE NEW LEVEL
	var levellevel = LevelLevel.level_empty()
	levellevel = LevelLevel.add_root(levellevel, LevelLevel.root(Col.RED, Col.BLACK))
	assert(LevelLevel.level_graph(levellevel)[0] == [], "Root should not have any child")
	levellevel = LevelLevel.add_sink(levellevel,
		LevelLevel.sink(
			LevelLevel.mods(Col.RED, Col.BLACK, LevelLevel.SUB, LevelLevel.SUB),
			LevelLevel.edge_eq(Col.WHITE, Col.WHITE)
		), 0)
	assert(LevelLevel.level_graph(levellevel)[0] == [1], "Root should now have a child")
	assert(LevelLevel.level_graph(levellevel)[1] == [], "Sink should not have any child")
	levellevel = LevelLevel.unlink(levellevel, 0, 1)
	assert(LevelLevel.level_graph(levellevel)[0] == [], "Root should not have any child")
	assert(LevelLevel.is_root(LevelLevel.level_nodes(levellevel)[1]), "Sink should now be root")
	levellevel = LevelLevel.link(levellevel, 1, 0)
	assert(LevelLevel.is_sink(LevelLevel.level_nodes(levellevel)[0]), "Old root is now a sink")
	assert(LevelLevel.level_graph(levellevel)[1] == [0], "Old sink now has old root as a child")
	levellevel = LevelLevel.add_sink(levellevel,
		LevelLevel.sink(
			LevelLevel.mods(Col.BLACK, Col.RED, LevelLevel.ADD, LevelLevel.ADD),
			LevelLevel.edge_eq(Col.BLACK, Col.RED)
		), 0)
	assert(LevelLevel.is_vert(LevelLevel.level_nodes(levellevel)[0]), "Old root is now vertex")
	levellevel = LevelLevel.link(levellevel, 2, 0)
	assert(LevelLevel.level_graph(levellevel)[2] == [], "Creating a loop is not possible")
	var propagated := LevelLevel.nodes_propagate(levellevel, Graph.topo(LevelLevel.level_graph(levellevel)).ok)
	assert(LevelLevel.cols_c1(LevelLevel.edge_colors_of(1, propagated)) == Col.RED, "Incorrect propagation of colors")
	assert(LevelLevel.cols_c2(LevelLevel.edge_colors_of(1, propagated)) == Col.BLACK, "Incorrect propagation of colors")
	assert(LevelLevel.cols_c1(LevelLevel.edge_colors_of(0, propagated)) == Col.RED, "Incorrect propagation of colors")
	assert(LevelLevel.cols_c2(LevelLevel.edge_colors_of(0, propagated)) == Col.BLACK, "Incorrect propagation of colors")
	assert(LevelLevel.cols_c1(LevelLevel.edge_colors_of(2, propagated)) == Col.BLACK, "Incorrect propagation of colors")
	assert(LevelLevel.cols_c2(LevelLevel.edge_colors_of(2, propagated)) == Col.BLACK, "Incorrect propagation of colors")
	
	
	# COLOR MATH 
	assert(col_eq(Col.op(Col.ADD, Col.RED).call(Col.YELLOW), Col.ORANGE))
	assert(col_eq(Col.op(Col.SUB, Col.YELLOW).call(Col.RED), Col.RED))
	assert(col_eq(Col.op(Col.ADD, Col.ORANGE).call(Col.YELLOW), Col.ORANGE))
	assert(col_eq(Col.op(Col.SUB, Col.RED).call(Col.ORANGE), Col.YELLOW))
	assert(col_eq(Col.op(Col.ADD, Col.ORANGE).call(Col.GREEN), Col.BLACK))
	assert(col_eq(Col.op(Col.SUB, Col.ORANGE).call(Col.ORANGE), Col.WHITE))
	assert(col_eq(Col.op(Col.SUB, Col.PURPLE).call(Col.ORANGE), Col.YELLOW))
	assert(col_eq(Col.op(Col.SUB, Col.RED).call(Col.RED), Col.WHITE))

	# COLORS PROPAGATION
	var add_sub_simple_graph = Level.propagate2({
		nodes = [
			Level.box2(Col.ORANGE, Col.GREEN, Level.mod_add(Col.BLUE), Level.mod_sub(Col.GREEN)),
			Level.box2(Col.WHITE, Col.BLACK, Level.mod_is(Col.BLACK), Level.mod_is(Col.WHITE)),
		],
		graph = [[1], []]
	}, {order = Graph.topo([[1], []]).ok})
	assert(col_eq(add_sub_simple_graph.nodes[1].color_a, Col.BLACK))
	assert(col_eq(add_sub_simple_graph.nodes[1].color_b, Col.WHITE))

	var propagate_req = Level.propagate2({
		nodes = [
			Level.box2(Col.ORANGE, Col.GREEN, Level.mod_any(), Level.mod_any()),
			Level.box2(Col.BLACK, Col.PURPLE, Level.mod_sub(Col.RED), Level.mod_add(Col.YELLOW)),
		],
		graph = [[1], []]
	}, {order = Graph.topo([[1], []]).ok})
	assert(col_eq(propagate_req.nodes[1].color_a, Col.ORANGE))
	assert(col_eq(propagate_req.nodes[1].color_b, Col.GREEN))

	var mod_2_levels = Level.propagate2({
		nodes = [
			Level.box2(Col.ORANGE, Col.GREEN, Level.mod_add(Col.BLUE), Level.mod_add(Col.BLUE)),
			Level.box2(Col.ORANGE, Col.WHITE, Level.mod_sub(Col.GREEN), Level.mod_sub(Col.GREEN)),
			Level.box2(Col.WHITE, Col.BLACK, Level.mod_is(Col.RED), Level.mod_is(Col.WHITE))
		],
		graph = [[1], [2], []]
	}, {order = Graph.topo([[1], [2], []]).ok}).nodes
	assert(col_eq(mod_2_levels[1].color_a, Col.BLACK))
	assert(col_eq(mod_2_levels[1].color_b, Col.GREEN))
	assert(col_eq(mod_2_levels[2].color_a, Col.RED))
	assert(col_eq(mod_2_levels[2].color_b, Col.WHITE))

	var init_mod_2_ways = [
		Level.box2(Col.PURPLE, Col.YELLOW, Level.mod_sub(Col.ORANGE), Level.mod_add(Col.BLUE)),
		Level.box2(Col.PURPLE, Col.YELLOW, Level.mod_add(Col.WHITE), Level.mod_sub(Col.BLACK)),
		Level.box2(Col.BLACK, Col.BLUE, Level.mod_is(Col.WHITE), Level.mod_is(Col.WHITE)),
		Level.box2(Col.GREEN, Col.ORANGE, Level.mod_is(Col.WHITE), Level.mod_is(Col.WHITE)),
		Level.box2(Col.PURPLE, Col.RED, Level.mod_is(Col.WHITE), Level.mod_is(Col.WHITE)),
	]
	var mod_2_ways = Level.propagate2({
		nodes = init_mod_2_ways,
		graph = [[1, 2], [3, 4], [], [], []],
	}, {order = Graph.topo([[1, 2], [3, 4], [], [], []]).ok}
	).nodes
	assert(col_eq(mod_2_ways[1].color_a, Col.BLUE))
	assert(col_eq(mod_2_ways[1].color_b, Col.GREEN))
	assert(col_eq(mod_2_ways[2].color_a, Col.BLUE))
	assert(col_eq(mod_2_ways[2].color_b, Col.GREEN))
	assert(col_eq(mod_2_ways[3].color_a, Col.BLUE))
	assert(col_eq(mod_2_ways[3].color_b, Col.WHITE))
	assert(col_eq(mod_2_ways[4].color_a, Col.BLUE))
	assert(col_eq(mod_2_ways[4].color_b, Col.WHITE))

	assert(init_mod_2_ways[0].color_a == Col.PURPLE, "Initial graph is not intact.")
	assert(init_mod_2_ways[0].color_b == Col.YELLOW, "Initial graph is not intact.")
	assert(init_mod_2_ways[1].color_a == Col.PURPLE, "Initial graph is not intact.")
	assert(init_mod_2_ways[1].color_b == Col.YELLOW, "Initial graph is not intact.")
	assert(init_mod_2_ways[2].color_a == Col.BLACK, "Initial graph is not intact.")
	assert(init_mod_2_ways[2].color_b == Col.BLUE, "Initial graph is not intact.")
	assert(init_mod_2_ways[3].color_a == Col.GREEN, "Initial graph is not intact.")
	assert(init_mod_2_ways[3].color_b == Col.ORANGE, "Initial graph is not intact.")
	assert(init_mod_2_ways[4].color_a == Col.PURPLE, "Initial graph is not intact.")
	assert(init_mod_2_ways[4].color_b == Col.RED, "Initial graph is not intact.")

	# MERGES IN COLOR PROPAGATION
	var merged := Level.propagate2({
		nodes = [
			Level.box2(Col.GREEN, Col.ORANGE, Level.mod_is(Col.GREEN), Level.mod_is(Col.ORANGE)),
			Level.box2(Col.PURPLE, Col.YELLOW, Level.mod_is(Col.PURPLE), Level.mod_is(Col.YELLOW)),
			Level.box2(Col.YELLOW, Col.BLUE, Level.mod_is(Col.BLACK), Level.mod_is(Col.ORANGE))
		],
		graph = [[2], [2], []],
	}, {order = Graph.topo([[2], [2], []]).ok})
	assert(col_eq(merged.nodes[2].color_a, Col.BLACK))
	assert(col_eq(merged.nodes[2].color_b, Col.ORANGE))

	var merged_three := Level.propagate2({
		nodes = [
			Level.box2(Col.GREEN, Col.BLUE, Level.mod_is(Col.GREEN), Level.mod_is(Col.BLUE)),
			Level.box2(Col.PURPLE, Col.RED, Level.mod_is(Col.PURPLE), Level.mod_is(Col.RED)),
			Level.box2(Col.RED, Col.WHITE, Level.mod_is(Col.RED), Level.mod_is(Col.WHITE)),
			Level.box2(Col.YELLOW, Col.WHITE, Level.mod_is(Col.BLACK), Level.mod_is(Col.PURPLE))
		],
		graph = [[2], [2], [], [2]],
	}, {order = Graph.topo([[2], [2], [], [2]]).ok})
	assert(col_eq(merged_three.nodes[2].color_a, Col.BLACK))
	assert(col_eq(merged_three.nodes[2].color_b, Col.PURPLE))

	# MODIFIER MANIPULATIONS
	var switch_parent: Dictionary = Level.propagate2({
		nodes = [
			Level.box2(Col.GREEN, Col.GREEN, Level.mod_add(Col.RED), Level.mod_sub(Col.YELLOW)),
			Level.box2(Col.WHITE, Col.WHITE, Level.mod_is(Col.BLUE), Level.mod_is(Col.BLACK))
		],
		graph = [[1], []],
	}, {order = Graph.topo([[1], []]).ok})
	var switched_parent: Dictionary = Level.switch_modifiers2(0).call(switch_parent)
	assert(col_eq(switch_parent.nodes[0].color_a, Col.GREEN))
	assert(col_eq(switch_parent.nodes[0].color_b, Col.GREEN))
	assert(col_eq(switch_parent.nodes[1].color_a, Col.BLACK))
	assert(col_eq(switch_parent.nodes[1].color_b, Col.BLUE))
	assert(col_eq(switched_parent.nodes[1].color_a, Col.BLUE))
	assert(col_eq(switched_parent.nodes[1].color_b, Col.BLACK))

	# VERTEX MANIPULATIONS
	var add_vertex: Array = Graph.empty()
	var many_vertices: Array = Graph.with([
		Graph.add_vertex(), Graph.add_vertex(), Graph.add_vertex()
	], add_vertex)
	assert(Graph.add_vertex().call(add_vertex)[0].is_empty())
	assert(add_vertex.size() == 0, "Initial graph is not intact.")
	assert(many_vertices.size() == 3)

	var remove_vertex: Array = [[1, 2], [3, 4], [], [4], []]
	var empty_graph: Array = Graph.with([
		Graph.remove_vertex(4), Graph.remove_vertex(2), Graph.remove_vertex(1),
		Graph.remove_vertex(0), Graph.remove_vertex(0)
	], remove_vertex)
	assert(Graph.remove_vertex(1).call(remove_vertex)[0] == [1])
	assert(Graph.remove_vertex(1).call(remove_vertex)[2] == [3])
	assert(empty_graph.is_empty(), "Every vertex should be removed.")
	assert(not remove_vertex.is_empty(), "Initial graph is not intact.")

	# EDGE MANIPULATIONS
	var add_edge: Dictionary = {
		nodes = [
			Level.box2(Col.BLACK, Col.BLUE, Level.mod_add(Col.WHITE), Level.mod_sub(Col.BLACK)),
			Level.box2(Col.RED, Col.YELLOW, Level.mod_sub(Col.RED), Level.mod_any()),
			Level.box2(Col.PURPLE, Col.GREEN, Level.mod_add(Col.GREEN), Level.mod_is(Col.PURPLE)),
		],
		graph = [[], [], []],
	}
	var many_edges: Dictionary = Level.with([
		Level.link(1, 0), Level.link(1, 2), Level.link(2, 0)
	], add_edge)
	assert(Level.link(0, 1).call(add_edge).graph[0] == [1])
	assert(add_edge.graph[0].is_empty(), "Initial graph is not intact.")
	assert(many_edges.graph.size() == 3)
	assert(Graph.topo(many_edges.graph).ok == [1, 2, 0])
	assert(col_eq(Level.link(0, 1).call(add_edge).nodes[1].color_a, Col.BLACK))
	assert(col_eq(Level.link(0, 1).call(add_edge).nodes[1].color_b, Col.WHITE))
	assert(col_eq(many_edges.nodes[0].color_a, Col.GREEN))
	assert(col_eq(many_edges.nodes[0].color_b, Col.YELLOW))
	assert(col_eq(many_edges.nodes[1].color_a, Col.RED))
	assert(col_eq(many_edges.nodes[1].color_b, Col.YELLOW))
	assert(col_eq(many_edges.nodes[2].color_a, Col.WHITE))
	assert(col_eq(many_edges.nodes[2].color_b, Col.YELLOW))

	var remove_edge: Array = [[1, 2], [], [1]]
	var remove_2_1: Array = Graph.remove_edge(2, 1).call(remove_edge)
	var remove_5_6: Array = Graph.remove_edge(5, 6).call(remove_edge)
	assert(remove_2_1[2] == [])
	assert(Graph.remove_edge(2, 1).call(remove_2_1)[2] == [])
	assert(remove_5_6[0] == [1, 2])
	assert(remove_5_6[1] == [])
	assert(remove_5_6[2] == [1])
	assert(remove_edge[2] == [1], "Initial graph is not intact.")

	# TOPOLOGICAL SORTING
	var two_unsorted_vertices: Array = [[1], []]
	var three_unsorted_vertices: Array = [[], [0, 2], [0]]
	var five_unsorted_vertices: Array = [[], [], [4, 1, 3], [0], [1]]
	assert(Graph.topo([]).ok == []) # empty case
	assert(Graph.topo(two_unsorted_vertices).ok == [0, 1])
	assert(Graph.topo(three_unsorted_vertices).ok == [1, 2, 0])
	assert(Graph.topo(five_unsorted_vertices).ok == [2, 4, 3, 1, 0])

	var end := Time.get_ticks_usec()
	print("All tests passed in " + str(end - start) + " µs")
