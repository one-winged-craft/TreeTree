class_name LevelLevel



## Constructors


const ADD = 0
const SUB = 1


static func level(graph: Array, nodes: Array) -> Dictionary:
	assert(graph.size() == nodes.size(), "Graph and nodes must be of equal size.")
	return {graph = graph, nodes = nodes}


static func level_empty() -> Dictionary:
	return level(Graph.empty(), [])


static func root(c1: int, c2: int, child_count := 0) -> Dictionary:
	return {root = {c1 = c1, c2 = c2, child_count = child_count}}


static func vert(m: Dictionary, edges_metas: Array) -> Dictionary:
	return {vert = {mods = m, edges = edges_metas}}


static func sink(modifiers: Dictionary, edge_metas: Dictionary) -> Dictionary:
	return {sink = {mods = modifiers, edge = edge_metas}}


static func mods(c1: int, c2: int, o1: int, o2: int) -> Dictionary:
	return {c1 = c1, c2 = c2, o1 = o1, o2 = o2}


# Hold a color pair
static func cols(c1: int, c2: int) -> Dictionary:
	return {c1 = c1, c2 = c2}


# Hold a color pair associated with a vertex
static func edge_colors_vertex(from: int, to: int, cs: Dictionary) -> Dictionary:
	return {vertex = {from = from, to = to, cols = cs}}


# Hold a color pair associated with a sink
static func edge_colors_sink(sid: int, cs: Dictionary) -> Dictionary:
	return {sink = {sink = sid, cols = cs}}


# Match any color on both slots (`c1` and `c2`).
static func edge_any() -> Dictionary:
	return {any = null}


# Match specific colors on both slots (`c1` and `c2`).
static func edge_eq(c1: int, c2: int) -> Dictionary:
	return {eq = {c1 = c1, c2 = c2}}


# Match a specific color on `c1` and anything on `c2`.
static func edge_eq1(c1: int) -> Dictionary:
	return {eq1 = c1}


# Match a specific color on `c2` and anything on `c1`.
static func edge_eq2(c2: int) -> Dictionary:
	return {eq2 = c2}



## Getters


static func cols_c1(cs: Dictionary) -> int:
	return cs.c1


static func cols_c2(cs: Dictionary) -> int: 
	return cs.c2


static func level_graph(lvl: Dictionary) -> Array:
	return lvl.graph


static func level_graph_children(lvl: Dictionary, id: int) -> Array:
	return lvl.graph[id]


static func level_nodes(lvl: Dictionary) -> Array:
	return lvl.nodes


static func level_node(lvl: Dictionary, id: int) -> Dictionary:
	return lvl.nodes[id]


static func level_size(lvl: Dictionary) -> int:
	return level_graph(lvl).size()


static func mods_c1(m: Dictionary) -> int:
	return m.c1


static func mods_c2(m: Dictionary) -> int:
	return m.c2


static func mods_o1(m: Dictionary) -> int:
	return m.o1


static func mods_o2(m: Dictionary) -> int:
	return m.o2


static func node_c1(node: Dictionary) -> int:
	return node_bind(node, 
		func(c1: int, _c2, _c) -> int: return c1,
		func(m: Dictionary, _es) -> int: return mods_c1(m),
		func(m: Dictionary, _e) -> int: return mods_c1(m)
	)


static func node_c2(node: Dictionary) -> int:
	return node_bind(node, 
		func(_c1, c2: int, _c) -> int: return c2,
		func(m: Dictionary, _es) -> int: return mods_c2(m),
		func(m: Dictionary, _e) -> int: return mods_c2(m)
	)


static func node_mods(node: Dictionary) -> Dictionary:
	return node_bind(node,
		func(_c1, _c2, _cc) -> Dictionary:
			assert(false, "Cannot get mods of root")
			return {},
		func(m: Dictionary, _es) -> Dictionary: return m,
		func(m: Dictionary, _es) -> Dictionary: return m
	)


static func edge_colors_from(ec: Dictionary) -> int:
	match ec:
		{ "vertex": var x }: return x.from
		{ "sink": var x }: return x.sink
		_: assert(false, "Should be an edge colors")
	return -1


static func edge_colors_cols(ec: Dictionary) -> Dictionary:
	match ec:
		{ "vertex": var x }: return x.cols
		{ "sink": var x }: return x.cols
		_: assert(false, "Should be an edge colors")
	return {}


## Predicates


static func is_root(node: Dictionary) -> bool:
	return node.has("root")


static func is_vert(node: Dictionary) -> bool:
	return node.has("vert")


static func is_sink(node: Dictionary) -> bool:
	return node.has("sink")



## Level manipulation


static func mods_apply(m: Dictionary, cs: Dictionary) -> Dictionary:
	var apply := func(op: int, a: int, b: int) -> int:
		match op:
			ADD: return Col.add(a, b)
			SUB: return Col.sub(a, b)
			_: assert(false, "invalid color operation")
		return -1
	return cols(
		apply.call(mods_o1(m), cols_c1(cs), mods_c1(m)),
		apply.call(mods_o2(m), cols_c2(cs), mods_c2(m))
	)


static func add_edge(edges_metas: Array, edge_metas: Dictionary) -> Array:
	return edges_metas + [edge_metas]


static func remove_edge(edge_metas: Array, at: int) -> Array:
	var result := edge_metas.duplicate()
	result.remove_at(at)
	return result


static func edge_colors_of(id: int, edges_colors: Array) -> Dictionary:
	var result := Fun.find_by(edges_colors,
		func(edge_colors: Dictionary) -> bool:
			return edge_colors_from(edge_colors) == id
	)
	assert(not edges_colors.is_empty(), "Each node should have propagated colors")
	return edge_colors_cols(result[0])


# Create link between a node (either root, vertex or sink) and make it
# a parent of something else, unknown to this function.
#
# WARNING: This function assumes the connection doesn't already exists.
# Ignoring this warning may lead to incorrect data (e.g. a mismatch
# between the number of edges and the number of associated metas).
static func node_add_child(src: Dictionary, edge_meta: Dictionary) -> Dictionary:
	return node_bind(src,
		func(c1, c2, child_count) -> Dictionary: return root(c1, c2, child_count + 1),
		func(m: Dictionary, edges_metas: Array) -> Dictionary:
			return vert(m, add_edge(edges_metas, edge_meta)),
		func(m: Dictionary, edge_metas: Dictionary) -> Dictionary: return vert(m, [edge_metas])
	)


# Transform this node so that it has a parent when it is still a root. It
# doesn't have (nor does it need) any information about that parent.
#
# WARNING: Should be used in conjunction with `node_add_child`. Ignoring this
# warning may result in root nodes not registered as such.
static func node_add_parent(dst: Dictionary, m: Dictionary) -> Dictionary:
	return node_bind(dst,
		func(c1: int, c2: int, child_count: int) -> Dictionary:
			if child_count == 0: return sink(m, edge_eq(c1, c2))
			return vert(m, Fun.repeat(child_count, edge_eq(c1, c2))),
		func(_m, _es) -> Dictionary: return dst, # keep reference
		func(_m, _e) -> Dictionary: return dst # keep reference
	)


static func node_bind(n: Dictionary, fr: Callable, fv: Callable, fs: Callable):
	match n:
		{ "root": var r }: return fr.call(r.c1, r.c2, r.child_count)
		{ "vert": var v }: return fv.call(v.mods, v.edges)
		{ "sink": var s }: return fs.call(s.mods, s.edge)
		_: assert(false, "unreachable")
	return null


static func node_update_at(nodes: Array, idx: int, f: Callable) -> Array:
	assert(idx < nodes.size(), "node_update_at: index is out of range.")
	var result := nodes.duplicate()
	result[idx] = f.call(nodes[idx])
	return result


# FIXME: `make_vertex` should not allow insertion of multiple vertices into the graph
static func add_item(lvl: Dictionary, make_vertex: Array[Callable], new_node: Dictionary) -> Dictionary:
	var new_graph := Graph.with(make_vertex, level_graph(lvl))
	match Graph.topo(new_graph):
		{ "ok": _ }: return level(new_graph, level_nodes(lvl) + [new_node])
		{ "err": _ }, _: return lvl


static func add_root(lvl: Dictionary, new_root: Dictionary) -> Dictionary:
	return add_item(lvl, [Graph.add_vertex()], new_root)


static func add_sink(lvl: Dictionary, new_sink: Dictionary, parent: int) -> Dictionary:
	assert(parent < level_size(lvl), "`parent' not in graph.")
	return link(add_root(lvl, new_sink), parent, level_size(lvl))


static func link(lvl: Dictionary, src: int, dst: int) -> Dictionary:
	assert(src < level_size(lvl), "`src' not in graph.")
	assert(dst < level_size(lvl), "`dst' not in graph.")
	if Graph.vertex_has_child(level_graph(lvl), src, dst): return lvl # Link exists
	var graph := Graph.with([Graph.add_edge(src, dst)], level_graph(lvl))
	match Graph.topo(graph):
		{ "ok": _ }:
			return level(graph,
				node_update_at(
				node_update_at(
					level_nodes(lvl),
					src, func(n: Dictionary) -> Dictionary:
						return node_add_child(n, edge_any())),
					dst, func(n: Dictionary) -> Dictionary:
						return node_add_parent(n, mods(Col.WHITE, Col.WHITE, ADD, ADD)))
			)
		{ "err": _ }, _: return lvl


static func unlink(lvl: Dictionary, src: int, dst: int) -> Dictionary:
	assert(src < level_size(lvl), "`src' not in graph.")
	assert(dst < level_size(lvl), "`dst' not in graph.")
	if not Graph.vertex_has_child(level_graph(lvl), src, dst): return lvl # Link does not exist
	var graph := Graph.with([Graph.remove_edge(src, dst)], level_graph(lvl))
	match Graph.topo(graph):
		{ "ok": var order }:
			var eid: int = level_graph(lvl)[src].find(dst)
			return level(graph,
				node_update_at(
				node_update_at(
					level_nodes(lvl),
					src, func(n: Dictionary) -> Dictionary:
						return node_bind(n, 
							func(c1: int, c2: int, child_count: int) -> Dictionary:
								return root(c1, c2, max(0, child_count - 1)),
							func(m: Dictionary, edges_metas: Array) -> Dictionary:
								return vert(m, remove_edge(edges_metas, eid)),
							func(_m, _e) -> Dictionary:
								assert(false, "Cannot remove child of sink.")
								return n)),
					dst, func(n: Dictionary) -> Dictionary:
						return node_bind(n, 
							func(_1, _2, _c) -> Dictionary:
								assert(false, "Cannot remove parent of root.")
								return n,
							func(m: Dictionary, edges_metas: Array) -> Dictionary:
								if dst in Graph.get_roots(graph, order):
									return root(m.c1, m.c2, edges_metas.size())
								return vert(m, edges_metas),
							func(m: Dictionary, edge_metas: Dictionary) -> Dictionary:
								if dst in Graph.get_roots(graph, order):
									return root(m.c1, m.c2, 0)
								return sink(m, edge_metas),
						))
			)
		{ "err": _ }, _: return lvl


static func nodes_propagate(lvl: Dictionary, topo: Array, result := []) -> Array:
	if topo.is_empty(): return result
	var id: int = topo[0]
	var node := level_node(lvl, id)
	var colors: Dictionary = cols(
		node_c1(node), node_c2(node)
	) if is_root(node) else mods_apply(node_mods(node), result.filter(
		func(edge_color: Dictionary) -> bool:
			return edge_color.has("vertex") and edge_color.vertex.to == id
	).reduce(
		func(pc: Dictionary, ec: Dictionary) -> Dictionary:
			return cols(Col.add(cols_c1(edge_colors_cols(ec)), cols_c1(pc)), Col.add(cols_c2(edge_colors_cols(ec)), cols_c2(pc))),
		cols(Col.WHITE, Col.WHITE)
	))
	return nodes_propagate(lvl, topo.slice(1),
		result + (
			[edge_colors_sink(id, colors)] if is_sink(node) else
			level_graph_children(lvl, id).map(
				func(cid: int) -> Dictionary: return edge_colors_vertex(id, cid, colors)
			)
		)
	)
